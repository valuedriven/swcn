var express = require('express');
var app = express();
var path = require('path');

app.get('/', function (req, res) {
  var remoteAddress = req.connection.remoteAddress
  var localAddress = req.connection.localAddress

  var waitTill = new Date(new Date().getTime() + 10);
  while(waitTill > new Date()){}

  res.sendFile(path.join(__dirname + '/index.html'));

})

app.listen(8080, function () {
  console.log('app listening on port 8080!')
})

